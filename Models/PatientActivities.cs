﻿using ServiceStack.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SD_Tema4.Models
{
    public class PatientActivities
    {
        [PrimaryKey]
        public int Id { get; set; }
        public int PatientId { get; set; }
        public int ActivityId { get; set; }
    }
}
