﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SD_Tema4.Models
{
    public class Patient
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Diagnostic { get; set; }
        public bool IsMedicineTaken { get; set; }
    }
}
